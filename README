grub-efi-install is a shell script which replaces grub-install to install GRUB and shim images in the EFI partition and update EFI boot variables.
It aims to be as compatible as possible with the grub-install binary provided in Debian grub2-common package while being more flexible and providing a number of differences/improvements.

Features:
- supported architectures: arm{el,hf}, arm64, amd64, i386, ia64, riscv64
- support encrypted boot directory if GRUB_ENABLE_CRYPTODISK=y
- check if efivarfs is mounted and writable
- generate CSV file with proper label and loader in EFI directory
- create GRUB environment block if it does not exist
- remove old grub and shim files in the EFI directory
- skip unnecessary update of EFI boot variables:
  - create boot entry only if it does not exist
  - move existing boot entry first in BootOrder if not first
- support multiple mounted EFI partitions (in a single or multiple --efi-directory options)
- support multiple unmounted EFI partition (as device arguments)
- install monolithic unsigned image if GRUB signed image is not available or --no-uefi-secure-boot
- build unsigned GRUB core image if monolithic unsigned image is not available
- support option --compress for modules, fonts and locales

Supported grub-install options:
--root-directory
--dir, -d
--boot-directory
--efi-directory
--locale-directory
--locales
--modules (when building the core image)
--bootloader-id
--target
--no-nvram
--removable
--force-extra-removable
--verbose, -v
--force (force first position in boot order)
--no-uefi-secure-boot
--uefi-secure-boot

Main differences with the original grub-install:
- default target is EFI even if EFI is not active
- do not build the core image (unused)
- do not install GRUB modules and font in the boot directory (embedded in signed image)
- remove old grub and shim files in the EFI directory
- generate CSV file with proper label (distributor) and file name (grub or shim) instead of use hardcoded file
- do not install CSV file in the removable media path
- do not install fallback in the distributor path
- install grub.cfg in the removable media path with --force-extra-removable and --no-nvram
- do not install grub image in removable media path with shim if --force-extra-removable and ! --no-nvram
- delete old EFI boot entries with the same label and path and different loader
- do not force first position in EFI boot order unless --force is passed
- select default target on x86: if EFI platform size cannot be read, use architecture size instead of i386
- install unsigned shim in the removable media path for fallback if secure boot installation is disabled
- always include GPT and MSDOS modules in core image to support expansion of Btrfs, LVM, RAID or ZFS on a new disk with a different partition table (see Debian Bug#1035317)
- always include FAT module in core image to read the EFI partition in rescue mode
